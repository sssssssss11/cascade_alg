from test_initializer import graph_init as g_i
from cascade_alg import cascade_alg as c_a


graph1 = g_i().method("Erdos", 5)
test1 = c_a(graph1)
print(test1.isStable(2))
print(test1.capital_dict)

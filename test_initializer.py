import networkx as nx
import random

class graph_init:

    __graph = []
    __liabilities_dict = {}
    __capital_dict = {0: 10, 1: 110, 2: 100, 3: 130, 4: 20}

    #RANDOM LIABILITY DICTINORY INITIALIZATION
    def __create_rand_dict(self, G, min, max):
        d = {}
        for i in nx.edges_iter(G):
            d.update({i: random.uniform(min, max)})
        return d

    #CREATE A RANDOM ERDOS GRAPH
    def __erdos_graph(self, node_num):
        self.__graph = nx.fast_gnp_random_graph(node_num, 0.5, seed=1, directed=True)
        self.__init_atr(self.__graph)
        return self.__graph

    #CREATE A RANDOM BARABASI GRAPH
    def __barabasi_graph(self, node_num):
        self.__graph = nx.barabasi_albert_graph(node_num, 4, seed=1)
        self.__init_atr(self.__graph)
        return self.__graph

    #SETTING CAPITAL ATRIBUTE FOR EACH NODE
    def __set_capital(self, graph):
        nx.set_node_attributes(graph,'capital', self.__capital_dict)

    #SETTING LIABILITIES ATRIBUTE FOT EACH EDGES
    def __set_liability(self, graph, min, max):
        self.__liabilities_dict = self.__create_rand_dict(graph, min, max)
        nx.set_edge_attributes(graph, "Liabilites", self.__liabilities_dict)

    #GETTING ATRIBUTE TO NODES AND EDGES
    def __init_atr(self, graph, min=50, max=100):
        self.__set_liability(graph, min, max)
        self.__set_capital(graph)

    def method(self, method_name, num, min=50, max=100):
        return {
            "Erdos": self.__erdos_graph(num),
            "Barabasi": self.__barabasi_graph(num),
        }[method_name]

#FOR TEST FUNCTION
def main():
    return 1
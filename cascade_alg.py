import networkx as nx

class cascade_alg:
    node_list = []
    capital_dict = {}
    edges_list = []

    def __init__(self, g):
        self.G = g
        self.node_list = list(nx.nodes(g))
        self.edges_list = list(nx.edges(g))
        self.capital_dict = nx.get_node_attributes(g, 'capital')

    #Checking our network
    def isStable(self, num):
        tmp_dict = self.loss(num)
        self.capital_dict = {i: tmp_dict[i] for i in tmp_dict if tmp_dict[i] != 0}

        if self.capital_dict:
            return True
        else:
            return False

    #loss cascade function. N is node where start a default
    def loss(self, N):
        self.node_list.pop(self.node_list.index(N))
        self.capital_dict[N] = 0

        for i in nx.edges(self.G, nbunch=N):

            #print("Edges ", i, " have liab: ", nx.get_edge_attributes(G_erdos, 'Liabilites')[i], " Node: ", i[0])

            if self.capital_dict[i[1]] != 0:
                self.capital_dict[i[1]] -= nx.get_edge_attributes(self.G, 'Liabilites')[i]

            #print("Capital of node",i[1]," is ", self.capital_dict[i[1]])

            self.edges_list.remove(i)
            if self.capital_dict[i[1]] < 0 and i[1] in self.node_list:
                self.loss(i[1])

        return self.capital_dict



#for test function
def main():
    return 1;